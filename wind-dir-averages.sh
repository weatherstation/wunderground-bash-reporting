#!/bin/bash
#
#
#

# Find all the Wind Directions and matching Wind Speed
declare -a DSarrayDValue
DSarrayDValue+=('DSarrayDValue')
declare -a DSarrayRValue
DSarraySValue+=('DSarrayRValue')
declare -a DSarraySValue
DSarraySValue+=('DSarraySValue')
declare -a DSarrayNSVec
DSarrayNSVec+=('DSarrayNSVec')
declare -a DSarrayEWVec
DSarrayEWVec+=('DSarrayEWVec')

function mvData {
  # Move Dir and SP into new array

  WinDir=${arrayValue[$i]}
  DSarrayDValue+=( $WinDir )
  # Radiants of DSarryDVale ( value * Pi / 180 )
  WRadiants=$(echo "scale=5; ${arrayValue[$i]} * 4 * a(1)/180" | bc -l)
  DSarrayRValue+=( $WRadiants )
  WinSpeed=${arrayValue[$si]}
  DSarraySValue+=( $WinSpeed )

  DSarrayNSVec+=( $(echo "scale=5; c($WRadiants)*$WinSpeed" | bc -l) )
  DSarrayEWVec+=( $(echo "scale=5; s($WRadiants)*$WinSpeed" | bc -l) )

#          echo "Unset ..."
  # Remove Dir and SP from old array
  unset arrayValue[$i]
  unset arrayValue[$si]
  unset arrayType[$i]
  unset arrayType[$si]
  unset arrayTimeTSNano[$i]
  unset arrayTimeTSNano[$si]
  unset arrayTimeTS[$i]
  unset arrayTimeTS[$si]
  unset arrayTopic[$i]
  unset arrayTopic[$si]

}

echo "Start ..."

if [[ "$1" == "" ]];then
  timeCheck="2m"
else
  timeCheck=$1
fi

readarray -t dataArray < <(curl -s -H "Accept: application/csv"  -G 'http://localhost:8086/query?pretty=false' \
 --data-urlencode "db=telegraf" --data-urlencode \
  "q=SELECT \"time\",\"topic\",\"value\" FROM \"mqtt_consumer\" \
  WHERE \"topic\" =~ /cbd5\/R\/WIN.*/ AND time > now() - $timeCheck ORDER by time")

declare -a arrayTimeTSNano
arrayTimeTSNano+=('timeTSNano')
declare -a arrayTimeTS
arrayTimeTS+=('timeTS')
declare -a arrayTopic
arrayTopic+=('topic')
declare -a arrayType
arrayType+=('type')
declare -a arrayValue
arrayValue+=('value')

topicSplit="/data/RFLINK/Acurite/cbd5/R/"


# ToDo - Check that we have results
#printf '%s\n' "${dataArray[0]}"
#name,tags,time,topic,value
#mqtt_consumer,,1536216714955269668,/data/RFLINK/Acurite/cbd5/R/WINDIR,112.5

# Split out the data into arrays
arrayLength=${#dataArray[@]}

orgDataLength=$arrayLength
orgData=(${dataArray[@]})

#echo $arrayLength

reduceTS=100000000
matchedTS=0
closeTS=0

for (( i=1; i<${arrayLength}; i++ ));do
#  echo $i
#  printf '%s\n' "${dataArray[$i]}"

  arrayTimeTSNano+=( $(cut -d',' -f3 <<< ${dataArray[$i]}) )
#  echo "${arrayTimeTSNano[$i]}"
  arrayTimeTS+=( $(echo "${arrayTimeTSNano[$i]} / $reduceTS" | bc ) )
#  echo "${arrayTimeTS[$i]}"

  arrayTopic+=( $(cut -d',' -f4 <<< ${dataArray[$i]}) )
#  echo "${arrayTopic[$i]}"

  arrayType+=( ${arrayTopic[$i]#"$topicSplit"} )
#  echo "${arrayType[$i]}"

  arrayValue+=( $(cut -d',' -f5 <<< ${dataArray[$i]}) )
#  echo "${arrayValue[$i]}"

done


arrayLength=${#arrayValue[@]}
#echo $arrayLength

i=1
while [ $i -lt $arrayLength ];do
#  echo " i = $i"

#  echo "${arrayType[$i]}"
#  echo "${arrayValue[$i]}"

  if [[ "${arrayType[$i]}" == "WINDIR" ]]; then
    # Find matching Speed by time
#    echo "Dir"
    SarrayLength=${#arrayType[@]}
#    echo $SarrayLength
    # Init TS
    sTS=-1
    sTSsi=-1

    for (( si=1; si<${SarrayLength}; si++ ));do
#      echo " si = $si"
      if [[ "${arrayType[$si]}" == "WINSP" ]]; then
        # Exact Time Stamp Match
        if [[ "${arrayTimeTS[$i]}" == "${arrayTimeTS[$si]}" ]]; then
          echo "Match at ${arrayTimeTS[$i]} for WinDIR ${arrayValue[$i]} and WinSP ${arrayValue[$si]}"
          mvData
          matchedTS=$[$matchedTS+1]

          # Leave si Loop
#          echo "Leaving ..."
          si=${SarrayLength}
          sTS=-2

          # Reset i Loop
          i=1
        else
#          echo "sTS = $sTS & sTSsi = $sTSsi"
#          echo "sATS = ${arrayTimeTS[$si]} & si = $si"

#          echo "WinDir TS ${arrayTimeTS[$i]}"
#          echo "WinSP  TS ${arrayTimeTS[$si]}"

          if [[ "$sTS" == "-1" ]]; then
#            echo "-1 "
#            echo "WinDir TS ${arrayTimeTS[$i]}"
#            echo "WinSP  TS ${arrayTimeTS[$si]}"
            sTimeSDiff=$(echo "scale=5; ${arrayTimeTS[$i]} - ${arrayTimeTS[$si]}" | bc -l)
            if [[ "$sTimeSDiff" -lt "0" ]]; then
#              echo "-1Neg Diff is $sTimeSDiff"
              sTSDTemp=$(echo "scale=5; $sTimeSDiff * -1" | bc -l)
              sTimeSDiff=$sTSDTemp
#              echo "TimeS Diff ( $sTimeSDiff )"
            fi
            sTS=$sTimeSDiff
#            echo "TimeS Diff = $sTimeSDiff"
          else
#            echo "-2 "

#            echo "WinDir TS ${arrayTimeTS[$i]}"
#            echo "WinSP  TS ${arrayTimeTS[$si]}"

            sTimeSDiff=$(echo "scale=5; ${arrayTimeTS[$i]} - ${arrayTimeTS[$si]}" | bc -l)
            if [[ "$sTimeSDiff" -lt "0" ]]; then
#              echo "-1Neg Diff is $sTimeSDiff"
              sTSDTemp=$(echo "scale=5; $sTimeSDiff * -1" | bc -l)
              sTimeSDiff=$sTSDTemp
            fi

            if [[ "$sTimeSDiff" -lt "$sTS" ]]; then
#              echo "Switch ..."
#              echo "Old $sTS & $sTSsi vs $sTimeSDiff "
              sTSsi=$si
              sTS=$sTimeSDiff
#              echo "New $sTS & $sTSsi for $sTimeSDiff "
            fi
          fi
        fi
      fi
    done

    # Match Odd Wind Dir with Wind Speed
    if [[ "$sTS" == "-1" ]]; then
      echo "Wind Error"
      printf '%s\n' "${orgData[@]}"
    elif [[ "$sTS" == "-2" ]]; then
#      echo "Match?"
      echo -n " "
    else
      si=$sTSsi
      echo "Close with $sTS diff at ${arrayTimeTS[$i]} for WinDIR ${arrayValue[$i]} and WinSP ${arrayValue[$si]}"
      mvData
      closeTS=$[$closeTS+1]
    fi

  fi
  i=$[$i+1]
done

#arrayLength=${#DSarrayDValue[@]}
#printf '%s\n' "${DSarrayDValue[@]}"
echo "Org Data Length $(( $orgDataLength-1  ))"
#printf '%s\n' "${orgData[@]}"
echo "Item Data Length $(( (${#DSarrayDValue[@]}-1)*2 )) "
echo "Combined Data Length $(( ${#DSarrayDValue[@]}-1 )) "
echo "Extra Wind Speed Data $(( ($orgDataLength-1) - ((${#DSarrayDValue[@]}-1)*2) )) "
echo "Exact TS Match $matchedTS"
echo "Closest TS Match $closeTS"
#printf '%s\n' "${DSarrayDValue[@]}"

echo "Sum of results ..."
sumAarrayLength=${#DSarrayDValue[@]}
sumNSval=0
sumEWval=0
for (( sumi=1; sumi<${sumAarrayLength}; sumi++ ));do
  sumNSval=$(echo "scale=5; $sumNSval + ${DSarrayNSVec[$sumi]}" | bc -l)
  sumEWval=$(echo "scale=5; $sumEWval + ${DSarrayEWVec[$sumi]}" | bc -l)
done

avgNSval=$(echo "scale=5; $sumNSval/$sumAarrayLength" | bc -l)
avgEWval=$(echo "scale=5; $sumEWval/$sumAarrayLength" | bc -l)

windSpeedAvg=$(echo "scale=5; sqrt(($avgNSval * $avgNSval) + ($avgEWval * $avgEWval))" | bc -l)

# =IF(TANH(H8/G8)>0;TANH(H8/G8)*180/PI();(TANH(H8/G8)+PI())*180/PI())
#=IF(TANH(H8/G8)>0
#  TANH(H8/G8)*180/PI()
#  (TANH(H8/G8)+PI())*180/PI())
winDirPTan=$(echo "scale=8; $avgEWval/$avgNSval" | bc -l)
#echo $winDirPTan
winDirTan=$(echo "scale=8; a($winDirPTan)" | bc -l)
#echo $winDirTan
if (( $(echo "$winDirTan > 0" | bc -l) )); then
#  echo "Pos"
  windDirAvg=$(echo "scale=8; $winDirTan * (180 / (4 * a(1)))" | bc -l)
else
#  echo "Neg"
  windDirAvg=$(echo "scale=8; ($winDirTan + (4 * a(1))) * (180 / (4 * a(1)))" | bc -l)
fi

echo "SumNS = $sumNSval & SumEW = $sumEWval "
echo "AvgNS = $avgNSval & AvgEW = $avgEWval "
echo "Wind Speed Average = $windSpeedAvg"
echo "Wind Direction Average = $windDirAvg"
