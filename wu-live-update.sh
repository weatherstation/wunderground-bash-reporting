#!/bin/bash
#
# WUnderground PWS live update
#  https://www.wunderground.com/personal-weather-station/mypws
#  http://wiki.wunderground.com/index.php/PWS_-_Upload_Protocol
#   https://feedback.weather.com/customer/en/portal/articles/2924682-pws-upload-protocol?b_id=17298
#

#
# curl -G 'http://localhost:8086/query?pretty=true' --data-urlencode "db=telegraf" --data-urlencode "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\"='/data/RFLINK/Acurite/cbd5/R/HUM' ORDER by time DESC LIMIT 1"
# curl -H "Accept: application/csv"  -G 'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\"='/data/RFLINK/Acurite/cbd5/R/HUM' ORDER by time DESC LIMIT 1"
# curl -G 'http://localhost:8086/query?pretty=true' --data-urlencode "db=telegraf" --data-urlencode "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\" =~ /\/data\/RFLINK\/Acurite\/.*\/R\/RAIN/ ORDER by time DESC LIMIT 1"

# cbd5
# 0bd5

function getTemp() {
#  timeout --foreground 30 mosquitto_sub -t "/data/RFLINK/Acurite/0bd5/R/TEMP"
  TempR=$(curl -s -H "Accept: application/csv"  -G 'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\" =~ /\/data\/RFLINK\/Acurite\/.*\/R\/TEMP/ ORDER by time DESC LIMIT 1" | tail -n1)

  #echo $TempR

  if [ -z "$TempR" ]; then
    return -1
  fi

  TempTS=$(echo $TempR | cut -d, -f3)

  #echo $TempTS
  # 1531205637817942592
  #echo $TempTS
  # date +%s%N
  TempTSH=$(date +"%Y-%m-%d_%Hh%Mm%S" -d@$(($TempTS / 1000000000)))

#  echo $TempTSH

  TempV=$(echo $TempR | cut -d, -f4)
  STempC=$TempV

#  echo $TempV
  return 0
}

function getHum {
#  timeout --foreground 30 mosquitto_sub -t "/data/RFLINK/Acurite/0bd5/R/HUM"
  HumR=$(curl -s -H "Accept: application/csv"  -G 'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\" =~ /\/data\/RFLINK\/Acurite\/.*\/R\/HUM/ ORDER by time DESC LIMIT 1" | tail -n1)

  if [ -z "$HumR" ]; then
    return -1
  fi

  HumTS=$(echo $HumR | cut -d, -f3)
  HumTSH=$(date +"%Y-%m-%d_%Hh%Mm%S" -d@$(($HumTS / 1000000000)))
  HumV=$(echo $HumR | cut -d, -f4)
  SHum=$HumV

  #echo $HumV
  return 0
}

function getDewPointTemperature {
# http://arduinotronics.blogspot.com/2013/12/temp-humidity-w-dew-point-calcualtions.html
#// delta max = 0.6544 wrt dewPoint()
#// 6.9 x faster than dewPoint()
#// reference: http://en.wikipedia.org/wiki/Dew_point
#double dewPointFast(double celsius, double humidity)
#{
# double a = 17.271;
# double b = 237.7;
# double temp = (a * celsius) / (b + celsius) + log(humidity*0.01);
# double Td = (b * temp) / (a - temp);
# return Td;
#}

#https://en.wikipedia.org/wiki/Dew_point
# a = 6.112 mb, b = 17.62, c = 243.12 °C; for �45 °C � T � 60 °C (error ±0.35 °C).

#TempC=24.7
#RHum=34
#DewTemp=7.79
#dewTa=17.271
#dewTb=237.1

  if [ -z "$STempC" ]; then
    return -1
  else
    TempC=$STempC
  fi

  if [ -z "$SHum" ]; then
    return -2
  else
    RHum=$SHum
  fi

#  TempC=$1
#  RHum=$2
#  echo "Temp $TempC"
#  echo "Hum $RHum"
  dewTa=17.62
  dewTb=243.12
  dewTtemp=$(echo "scale=2; ($dewTa * $TempC) / ($dewTb + $TempC) + l($RHum*0.01)" | bc -l)
  dewTemp=$(echo "scale=2; ($dewTb * $dewTtemp) / ($dewTa - $dewTtemp)" | bc -l)
  SdewPTempC=$dewTemp
#  echo $dewTemp

  return 0
}


function getWinDir {
#  timeout --foreground 30 mosquitto_sub -t "/data/RFLINK/Acurite/0bd5/R/WINDIR"
  WinDirR=$(curl -s -H "Accept: application/csv"  -G 'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\" =~ /\/data\/RFLINK\/Acurite\/.*\/R\/WINDIR/ ORDER by time DESC LIMIT 1" | tail -n1)

  if [ -z "$WinDirR" ]; then
    return -1
  fi

  WinDirTS=$(echo $WinDirR | cut -d, -f3)
  WinDirTSH=$(date +"%Y-%m-%d_%Hh%Mm%S" -d@$(($WinDirTS / 1000000000)))
  WinDirV=$(echo $WinDirR | cut -d, -f4)
  SWinDir=$WinDirV

  #echo $WinDirV
  return 0
}

function getWinSP {
#  timeout --foreground 30 mosquitto_sub -t "/data/RFLINK/Acurite/0bd5/R/WINSP"
  WinSPR=$(curl -s -H "Accept: application/csv"  -G 'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\" =~ /\/data\/RFLINK\/Acurite\/.*\/R\/WINSP/ ORDER by time DESC LIMIT 1" | tail -n1)

  if [ -z "$WinSPR" ]; then
    return -1
  fi

  WinSPTS=$(echo $WinSPR | cut -d, -f3)
  WinSPTSH=$(date +"%Y-%m-%d_%Hh%Mm%S" -d@$(($WinSPTS / 1000000000)))
  WinSPV=$(echo $WinSPR | cut -d, -f4)
  SWinSPKT=$WinSPV

  #echo $WinSPV
  return 0
}

function getWinSPA2m {
  WinSPA2mR=$(curl -s -H "Accept: application/csv" -G \
    'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" \
    --data-urlencode "q=SELECT MEAN(\"value\") FROM \"mqtt_consumer\" WHERE \"topic\" =~ /\/data\/RFLINK\/Acurite\/.*\/R\/WINSP/ AND time > now() - 2m ORDER by time DESC LIMIT 1" | tail -n1)

  if [ -z "$WinSPA2mR" ]; then
    return -1
  fi

  WinSPA2mTS=$(echo $WinSPA2mR | cut -d, -f3)
  WinSPA2mTSH=$(date +"%Y-%m-%d_%Hh%Mm%S" -d@$(($WinSPA2mTS / 1000000000)))
  WinSPA2mV=$(echo $WinSPA2mR | cut -d, -f4)
  SWinSPA2mKT=$WinSPA2mV

  #echo $WinSPA2mV
  return 0
}

function getRainH {
  RainHR=$(curl -s -H "Accept: application/csv" -G \
    'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode \
    "q=SELECT SPREAD(\"value\") FROM \"mqtt_consumer\" \
    WHERE \"topic\" =~ /\/data\/RFLINK\/Acurite\/.*\/R\/RAIN/ AND time > now() - 1h" | tail -n1)

  if [ -z "$RainHR" ]; then
    return -1
  fi

  RainHTS=$(echo $RainHR | cut -d, -f3)
  RainHTSH=$(date +"%Y-%m-%d_%Hh%Mm%S" -d@$(($RainHTS / 1000000000)))
  RainHV=$(echo $RainHR | cut -d, -f4)
  SRainHKT=$RainHV

  # Get last date/time
  RainHTSL=$(curl -s -H "Accept: application/csv" -G \
    'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode \
    "q=SELECT LAST(\"value\") FROM \"mqtt_consumer\" \
    WHERE \"topic\" =~ /\/data\/RFLINK\/Acurite\/.*\/R\/RAIN/ AND time > now() - 1h" | tail -n1 | cut -d, -f3)

  #echo $RainHV
  return 0
}

function getRainD {

  # Set date at midnight less timezone shift?
  datetest=$(echo $(date +"%Y-%m-%d" --date yesterday)"T22:00:00Z'")

  #echo $datetest

  RainDR=$(curl -s -H "Accept: application/csv" -G \
    'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode \
    "q=SELECT SPREAD(\"value\") FROM \"mqtt_consumer\" \
    WHERE \"topic\" =~ /0bd5\/R\/RAIN.*/ AND time >= '$datetest" | tail -n1)

  if [ -z "$RainDR" ]; then
    return -1
  fi

  RainDTS=$(echo $RainDR | cut -d, -f3)
  RainDTSH=$(date +"%Y-%m-%d_%Hh%Mm%S" -d@$(($RainDTS / 1000000000)))
  RainDV=$(echo $RainDR | cut -d, -f4)
  SRainDKT=$RainDV

  # Get last date/time
  RainDTSL=$(curl -s -H "Accept: application/csv" -G \
    'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode \
    "q=SELECT LAST(\"value\") FROM \"mqtt_consumer\" \
    WHERE \"topic\" =~ /0bd5\/R\/RAIN.*/ AND time >= '$datetest" | tail -n1 | cut -d, -f3)

  #echo $RainDV
  return 0
}

function getPressure {
#  PressureR=$(curl -s -H "Accept: application/csv"  -G 'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\"='/data/ESP_Easy_Gate/BMP280/Pressure' ORDER by time DESC LIMIT 1" | tail -n1)
#  PressureR=$(curl -s -H "Accept: application/csv"  -G 'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\"='/data/espeasy/ESP_Easy_SDM120/BMP280/Pressure' ORDER by time DESC LIMIT 1" | tail -n1)
  PressureR=$(curl -s -H "Accept: application/csv"  -G 'http://localhost:8086/query?pretty=false' \
    --data-urlencode "db=telegraf" --data-urlencode \
    "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\"='/data/espeasy/ESP_Easy_SDM120/BMx280/Pressure' ORDER by time DESC LIMIT 1" | tail -n1)

  if [ -z "$PressureR" ]; then
    return -1
  fi

  PressureTS=$(echo $PressureR | cut -d, -f3)
  PressureTSH=$(date +"%Y-%m-%d_%Hh%Mm%S" -d@$(($PressureTS / 1000000000)))
  PressureV=$(echo $PressureR | cut -d, -f4)
  SPressureV=$PressureV

  return 0
}

function getITemp {
#  ITempR=$(curl -s -H "Accept: application/csv"  -G 'http://localhost:8086/query?pretty=false' --data-urlencode "db=telegraf" --data-urlencode "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\"='/data/espeasy/ESP_Easy_SDM120/BMP280/Temperature' ORDER by time DESC LIMIT 1" | tail -n1)
  ITempR=$(curl -s -H "Accept: application/csv"  -G 'http://localhost:8086/query?pretty=false' \
    --data-urlencode "db=telegraf" --data-urlencode \
    "q=SELECT \"value\" FROM \"mqtt_consumer\" WHERE \"topic\"='/data/espeasy/ESP_Easy_SDM120/BMP280/Temperature' ORDER by time DESC LIMIT 1" | tail -n1)

  if [ -z "$ITempR" ]; then
    return -1
  fi

  ITempTS=$(echo $ITempR | cut -d, -f3)
  ITempTSH=$(date +"%Y-%m-%d_%Hh%Mm%S" -d@$(($ITempTS / 1000000000)))
  ITempV=$(echo $ITempR | cut -d, -f4)
  SITempC=$ITempV

  return 0
}

CurrentTimeStamp=$(date -u +'%Y-%m-%d_%Hh%Mm%S')
echo $CurrentTimeStamp
echo "Local - $(date +'%Y-%m-%d_%Hh%Mm%S')"

# Cron special
SCurlOp=""
if [ "$1" == "cron" ]; then
  shift
  SCurlOp=" -s "
fi

# Parm in Env
if [[ -n "$SID" && -n "$SPWD" ]]; then
  echo "Env $SID ..."
elif [[ -n "$1" && -n "$2" ]]; then
  SID=$1
  SPW=$2
  echo "Parm $1 as SID"
#if [ -x parms-config ]; then
elif [ -f parms-config ]; then
  # Used to include config/defaults in files
  echo "Including parms-config ..."
  source ./parms-config
else
  echo "No parms-config to include?"
  exit 1
fi

echo "Station ID: $SID"
#echo "Station Key: $SPW"

echo " . "

URL="https://weatherstation.wunderground.com/weatherstation/updateweatherstation.php"

# 2001-01-01 10:32:35
#   becomes
#  2000-01-01+10%3A32%3A35
# dateutc=2000-01-01+10%3A32%3A35

  #
  # date +%s%N
  # 1531205637817942592
  # 2018-07-10_08h53m57
  #echo $(date -u +"%Y-%m-%d+%H%%3A%M%%3A%S" -d@$(echo "1531205637817942592 / 1000000000" | bc -l))
  #2018-07-10+06%3A53%3A57
  #

SDate="now"

echo " .. "
SVar=""

#&tempf=
# Sent in F and recieved as C
#STempC=$(mosquitto_sub -h localhost -t "/data/RFLINK/Acurite/0bd5/R/TEMP")
# formula Tf=(9/5)*Tc+32
#  tf=$(echo "scale=2;((9/5) * $tc) + 32" |bc)

#STempC=$(getTemp)
#if [ -n "$STempC" ]; then
getTemp
#echo "$?"
if [[ "$?" -eq "0" ]]; then
  echo $STempC "C Temp" $TempTSH
  STempF=$(echo "scale=2;((9/5) * $STempC) + 32" | bc)
  echo $STempF "F Temp"
  curl $SCurlOp -i -XPOST 'http://localhost:8086/write?db=telegraf' --data-binary 'mqtt_consumer,host=nexus,topic=/data/RFLINK/Acurite/0bd5/C/TEMPF value='$STempF
  SVar="$SVar&tempf=$STempF"
fi

echo " ... "

#&humidity=
# Sent in % and recieved as %
#SHum=$(getHum)
#if [ -n "$SHum" ]; then
getHum
if [[ "$?" -eq "0" ]]; then
  echo $SHum "% Humidity" $HumTSH
  SVar="$SVar&humidity=$SHum"
fi

echo " .... "

#&dewptf=
#dewptf- [F outdoor dewpoint F]
#SdewPTempC=-20
getDewPointTemperature
#echo "$?"
if [[ "$?" -eq "0" ]]; then
  echo $SdewPTempC "C Dew Point Temp"
  curl $SCurlOp -i -XPOST 'http://localhost:8086/write?db=telegraf' --data-binary 'mqtt_consumer,host=nexus,topic=/data/RFLINK/Acurite/0bd5/C/DEWPOINTC value='$SdewPTempC
  SdewPTempF=$(echo "scale=2;((9/5) * $SdewPTempC) + 32" | bc)
  echo $SdewPTempF "F Dew Point Temp"
  curl $SCurlOp -i -XPOST 'http://localhost:8086/write?db=telegraf' --data-binary 'mqtt_consumer,host=nexus,topic=/data/RFLINK/Acurite/0bd5/C/DEWPOINTF value='$SdewPTempF
  SVar="$SVar&dewptf=$SdewPTempF"
fi

echo " ..... "

#&winddir=
# Sent in Deg and recieved as Deg
#SWinDir=$(getWinDir)
#if [ -n "$SWinDir" ]; then
getWinDir
if [[ "$?" -eq "0" ]]; then
  echo $SWinDir "\" Wind Direction" $WinDirTSH
  SVar="$SVar&winddir=$SWinDir"
fi

echo " ...... "

#&=windspeedmph
# Sent in MPH and recieved as KMP
# formula Tf=(9/5)*Tc+32
#  tf=$(echo "scale=2;((9/5) * $tc) + 32" |bc)
#SWinSPKT=$(getWinSP)
#if [ -n "$SWinSPKT" ]; then
getWinSP
if [[ "$?" -eq "0" ]]; then
  SWinSPK=$(echo "$SWinSPKT" | tail -n1)
  echo $SWinSPK "Km p/h Wind Speed" $WinSPTSH
  SWinSPM=$(echo "scale=2; $SWinSPK / 1.60934" | bc)
  echo $SWinSPM "M p/h Wind Speed"
  curl $SCurlOp -i -XPOST 'http://localhost:8086/write?db=telegraf' --data-binary 'mqtt_consumer,host=nexus,topic=/data/RFLINK/Acurite/0bd5/C/WINSPM value='$SWinSPM
  SVar="$SVar&windspeedmph=$SWinSPM"
fi

echo " ....... "

#&=windspdmph_avg2m
#windspdmph_avg2m  - [mph 2 minute average wind speed mph]
# Sent in MPH and recieved as KMP
# formula Tf=(9/5)*Tc+32
#  tf=$(echo "scale=2;((9/5) * $tc) + 32" |bc)
getWinSPA2m
if [[ "$?" -eq "0" ]]; then
  SWinSPA2mK=$(echo "$SWinSPA2mKT" | tail -n1)
  echo $SWinSPA2mK "2m average - Km p/h Wind Speed" $WinSPA2mTSH
  SWinSPA2mM=$(echo "scale=2; $SWinSPA2mK / 1.60934" | bc)
  echo $SWinSPA2mM "2m average - M p/h Wind Speed"
  curl $SCurlOp -i -XPOST 'http://localhost:8086/write?db=telegraf' --data-binary 'mqtt_consumer,host=nexus,topic=/data/RFLINK/Acurite/0bd5/C/WINSPA2mM value='$SWinSPA2mM
  SVar="$SVar&windspdmph_avg2m=$SWinSPA2mM"
fi

echo " ........ "

#&=rainin
# rainin - [rain inches over the past hour)] -- the accumulated rainfall in the past 60 min
# Sent in inches and recieved as mm
# formula Tf=Tc * 0.0394
#  tf=$(echo "scale=2;($tc * 0.0394)" |bc)
getRainH
if [[ "$?" -eq "0" ]]; then
  SRainHm=$(echo "$SRainHKT" | tail -n1)
  echo $SRainHm " mm Rain acculated over the last hour"
  SRainHI=$(echo "scale=2; $SRainHm * 0.0394" | bc)
  echo $SRainHI " in Rain acculated over the last hour"
  echo " from: " $RainHTSH
  echo "   to: " $(date +"%Y-%m-%d_%Hh%Mm%S" -d@$(($RainHTSL / 1000000000)))
  curl $SCurlOp -i -XPOST 'http://localhost:8086/write?db=telegraf' --data-binary 'mqtt_consumer,host=nexus,topic=/data/RFLINK/Acurite/0bd5/C/RAINHI value='$SRainHI
  SVar="$SVar&rainin=$SRainHI"
fi

echo " ......... "

#&=dailyrainin
# dailyrainin - [rain inches so far today in local time]
# Sent in inches and recieved as mm
# formula Tf=Tc * 0.0394
#  tf=$(echo "scale=2;($tc * 0.0394)" |bc)
getRainD
if [[ "$?" -eq "0" ]]; then
  SRainDm=$(echo "$SRainDKT" | tail -n1)
  echo $SRainDm " mm Rain acculated for the day"
  SRainDI=$(echo "scale=2; $SRainDm * 0.0394" | bc)
  echo $SRainDI " in Rain acculated for the day"
  echo " from: " $RainDTSH
  echo "   to: " $(date +"%Y-%m-%d_%Hh%Mm%S" -d@$(($RainDTSL / 1000000000)))
  curl $SCurlOp -i -XPOST 'http://localhost:8086/write?db=telegraf' --data-binary 'mqtt_consumer,host=nexus,topic=/data/RFLINK/Acurite/0bd5/C/RAINDI value='$SRainDI
  SVar="$SVar&dailyrainin=$SRainDI"
fi

echo " .......... "

#&=baromin
# Sent in PSI and recieved as kPa
# formula PpV=kPa * 0.14503773773020923
#  PpV=$(echo "scale=2;($kPa * 0.14503773773020923 )" |bc)
#SWinSPKT=$(getWinSP)
#if [ -n "$SWinSPKT" ]; then
getPressure
if [[ "$?" -eq "0" ]]; then
  SPressureVhPa=$(echo "$SPressureV" | tail -n1)
  echo $SPressureVhPa "barometric pressure hPa" $PressureTSH
#  SPressurePi=$(echo "scale=2; ($SPressureK * 0.14503773773020923)" | bc)
#  SPressurePi=$(echo "scale=2; ($SPressureK * 0.145)" | bc)
#  SPressurePmhPa=$(echo "scale=2; ($SPressureMPA / 99.96)" | bc)
#  SPressurePmhPa=$(echo "scale=2; ($SPressureMPA / 99.96)" | bc)
#  echo $SPressurePmhPa "barometric pressure hPa"
#  SPressureInHG=$(echo "scale=2; ($SPressurePmhPa * 0.02953)" | bc)
  SPressureInHG=$(echo "scale=2; ($SPressureVhPa * 0.02953)" | bc)
  echo $SPressureInHG "barometric pressure inHg"
  curl $SCurlOp -i -XPOST 'http://localhost:8086/write?db=telegraf' --data-binary \
    'mqtt_consumer,host=nexus,topic=/data/espeasy/ESP_Easy_SDM120/BMP280/C/PressureInHG value='$SPressureInHG
  SVar="$SVar&baromin=$SPressureInHG"
fi

echo " ........... "

#&=indoortempf - [F indoor temperature F]
#indoorhumidity - [% indoor humidity 0-100]
getITemp
if [[ "$?" -eq "0" ]]; then
  echo $SITempC "C indoor Temp" $ITempTSH
  SITempF=$(echo "scale=2;((9/5) * $SITempC) + 32" | bc)
  echo $SITempF "F indoor Temp"
  curl $SCurlOp -i -XPOST 'http://localhost:8086/write?db=telegraf' --data-binary \
    'mqtt_consumer,host=nexus,topic=/data/espeasy/ESP_Easy_SDM120/BMP280/C/TemperatureF value='$SITempF
  SVar="$SVar&indoortempf=$SITempF"
fi

echo " ............ "

# &action=updateraw

echo " ............. "

#echo -n $URL
#echo -n "?"
#echo -n "ID=$SID"
#echo -n "&PASSWORD=$SPW"
#echo -n "&dateutc=$SDate"
#echo -n "&tempf=$STempF"
#echo -n "&humidity=$SHum"
#echo -n "$SVar"
#echo -n "&action=updateraw"

echo

# softwaretype - [text] ie: WeatherLink, VWS, WeatherDisplay
SoftwareType="WU4accurite2bash2018"
SVar="$SVar&softwaretype=$SoftwareType"

SCurl=$URL"?ID=$SID&PASSWORD=$SPW&dateutc=$SDate"

echo -n "curl "
echo $SCurlOp$SCurl$SVar"&action=updateraw"

#curl $SCurlOp$SCurl$SVar"&action=updateraw"
echo " "
